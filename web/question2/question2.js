
// Holds data about Pokemon types and their effectiveness.
var typeData = [
    { name: "Fire", data: ["X", "X", "O", "", "O"] },
    { name: "Water", data: ["O", "X", "X", "", ""] },
    { name: "Grass", data: ["X", "O", "X", "", ""] },
    { name: "Electric", data: ["", "O", "X", "X", ""] },
    { name: "Ice", data: ["X", "X", "O", "", "X"] }
];

$(document).ready(function() {


    $('#table-header').append('<tr></tr>');
    $('#table-header > tr').append('<th> </th>');
    for(var x in typeData){
        $('#table-header > tr').append('<th>' +typeData[x].name + '</th>');
    }


    // TODO Question 2 C: Dynamically generate the contents of the table's <tbody>

    for(var x in typeData) {

       var innerString = "";
        for(var y in typeData[x].data) {
            var data = typeData[x].data[y];
            var color = "white";
            if (data == "X"){
                color = "pink";
            }
            if (data == "O"){
                color = "lightgreen;"
            }
            innerString += "<td style=\"background-color: " +color+ "\">" + data + "</td>";
console.log(innerString)
        }
        var newrow = '<tr id=" '+typeData[x].name+' "><th>' + typeData[x].name + '</th>' +innerString+ '</tr>'
        $('#table-body').append(newrow);
    }

});