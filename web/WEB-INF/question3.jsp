<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pokemon Type Chart</title>

    <link rel="stylesheet" href="./question2/question2.css"/>
</head>
<body>
<table class="my-table">
    <thead>
    <tr>
        <th>
            <a href="./question3?sortOrder=${nextOrder}">
                <c:choose>
                    <c:when test="${currentOrder eq 'asc'}">▲</c:when>
                    <c:when test="${currentOrder eq 'desc'}">▼</c:when>
                    <c:otherwise>↕</c:otherwise>
                </c:choose>
            </a>
        </th>

        <c:forEach var="type" items="${columnData}">
            <th>${type.name}</th>
        </c:forEach>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="type" items="${rowData}">

        <tr>
            <th>${type.name}</th>

            <c:forEach var="data" items="${type.data}">

                <td>${data}</td>

            </c:forEach>
        </tr>

    </c:forEach>
    </tbody>
</table>
</body>
</html>